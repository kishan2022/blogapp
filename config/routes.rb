Rails.application.routes.draw do
  root "blogs#index"
  resources :blogs
  get "/notFound", to: "blogs#notFound"
  get "/archived-blogs", to:"blogs#archivedBlogs"
end
