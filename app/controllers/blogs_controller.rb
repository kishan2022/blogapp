class BlogsController < ApplicationController
  def index
    @blogs = Blog.order(created_at: :desc)
  end

  def show
    @blog = Blog.find(params[:id])
    rescue ActiveRecord::RecordNotFound
    redirect_to :controller => "blogs", :action => "notFound"
    return
  end

  def new
    @blog = Blog.new
  end

  def create
    @blog = Blog.new(article_params)
    if @blog.save
      redirect_to @blog
    else
      render :new, status: :unprocessable_entity
    end
  end

  def edit
    @blog = Blog.find(params[:id])
  end

  def update
    @blog = Blog.find(params[:id])
    if @blog.update(article_params)
      redirect_to @blog
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    @blog = Blog.find(params[:id])
    @blog.destroy
    redirect_to blogs_path, status: :see_other
  end

  def archivedBlogs
    @blogs = Blog.where(status:'archived').order(created_at: :desc)
  end

  def notFound
  end

  private
  def article_params
    params.required(:blog).permit(:title,:body,:status)
  end

end
